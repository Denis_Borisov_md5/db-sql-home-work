SELECT
	avg(start_price), items_id AS "Лоты", full_name AS "Пользователь"
FROM 
	internetshop.items i
LEFT JOIN 
	internetshop.users u
ON 
	i.users_user_id = u.user_id
GROUP BY
	full_name;