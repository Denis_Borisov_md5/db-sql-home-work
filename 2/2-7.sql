SELECT
	items_id AS "Лоты", full_name AS "Пользователь"
FROM 
	internetshop.items i
LEFT JOIN 
	internetshop.users u
ON 
	i.users_user_id = u.user_id
WHERE
	i.start_date <= now() and i.stop_date >= now()
GROUP BY
	full_name;