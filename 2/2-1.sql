SELECT
	bid_id AS "Ставки", full_name AS "Пользователь"
FROM 
	internetshop.bids b
LEFT JOIN 
	internetshop.users u
ON 
	b.users_user_id = u.user_id
WHERE
	users_user_id = 5;