SELECT
	items_id AS "Лоты", title AS "Наименование", description AS "Описание", max(bid_value) AS "Максимальная ставка"
FROM 
	internetshop.items i
LEFT JOIN 
	internetshop.bids b
ON 
	i.items_id = b.items_items_id
GROUP BY
	items_items_id;