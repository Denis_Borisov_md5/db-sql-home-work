SELECT
	name, count(mark)
FROM 
	students.marks m
LEFT JOIN 
	students.student s
ON 
	m.student_id = s.student_id
GROUP BY
	name;