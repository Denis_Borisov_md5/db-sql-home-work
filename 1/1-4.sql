SELECT 
	avg(mark), name
FROM 
	students.marks m
LEFT JOIN 
	students.student s
ON 
	m.student_id = s.student_id
WHERE
	s.name = "Петров Агафон";